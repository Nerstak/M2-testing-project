package models;

import com.microsoft.playwright.Page;
import utils.Constants;

public class EditEmployees {
    private final Page page;

    public EditEmployees(Page page) {
        this.page = page;
    }

    public void navigate() {
        page.navigate(Constants.URL_WEBSITE_PREP_ROD + "/employees");
    }

    public void editEmployee(String name) {
        page.click("tr:has(td:text(\" " + name + "\")) >> text=\"Edit\"");
    }

    public void startPromote() {
        page.click("text=\"Promote as manager\"");
    }

    public void validatePromote() {
        page.click("text=\"Proceed\"");
    }
}

