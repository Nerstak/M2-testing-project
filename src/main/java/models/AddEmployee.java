package models;

import com.microsoft.playwright.Page;
import utils.Constants;

import java.util.HashMap;

public class AddEmployee {
    private final Page page;

    public AddEmployee(Page page) {
        this.page = page;
    }

    public void navigate() {
        page.navigate(Constants.URL_WEBSITE_PREP_ROD + "/add_employee");
    }

    public void fillFields(HashMap<String, String> employeeFields) {
        page.fill("input[name=\"name\"]", employeeFields.get("name"));
        page.fill("input[name=\"email\"]", employeeFields.get("email"));
        page.fill("input[name=\"address_line1\"]", employeeFields.get("address1"));
        page.fill("input[name=\"address_line2\"]", employeeFields.get("address2"));
        page.fill("input[name=\"city\"]", employeeFields.get("city"));
        page.fill("input[name=\"zip_code\"]", employeeFields.get("zipCode"));
        page.fill("input[name=\"hiring_date\"]", employeeFields.get("hiringDate"));
        page.fill("input[name=\"job_title\"]", employeeFields.get("jobTitle"));
    }

    public void addEmployee() {
        page.locator("button").click();
    }
}
