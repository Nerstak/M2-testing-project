package models;

import utils.BaseHttpRequestBuilder;
import utils.Constants;

import java.io.IOException;
import java.net.URISyntaxException;


public class AddEmployeeHttp {
    private static final String ADD_URI = "/add_employee";

    public AddEmployeeHttp() throws URISyntaxException {
    }

    public void addEmptyEmployee() throws IOException, InterruptedException {
        /*var values = new HashMap<String, String>() {{
        }};

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(values);*/

        new BaseHttpRequestBuilder(Constants.URL_WEBSITE_LOCAL).postFromString(ADD_URI,"");
    }
}
