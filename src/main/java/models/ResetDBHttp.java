package models;

import utils.Constants;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ResetDBHttp {
    private final URI resetURI = new URI(Constants.URL_WEBSITE_LOCAL + "/reset_db");

    public ResetDBHttp() throws URISyntaxException {
    }

    public void resetDB() {
        HttpRequest request = HttpRequest
                .newBuilder()
                .uri(resetURI)
                .DELETE()
                .build();
        HttpClient.newHttpClient().sendAsync(request, HttpResponse.BodyHandlers.ofString()).join();
    }
}
