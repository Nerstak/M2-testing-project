package models;

import com.microsoft.playwright.Page;
import utils.Constants;

public class ResetDB {
    private final Page page;

    public ResetDB(Page page) {
        this.page = page;
    }

    public void reset() {
        page.navigate(Constants.URL_WEBSITE_PREP_ROD + "/reset_db");
        page.click("text=\"Proceed\"");
    }
}
