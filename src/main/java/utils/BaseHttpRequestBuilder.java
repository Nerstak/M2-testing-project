package utils;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Map;

public class BaseHttpRequestBuilder {
    private String baseURL;

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    public BaseHttpRequestBuilder(String baseURL){
        this.baseURL = baseURL;
    }

    private HttpRequest.BodyPublisher ofFormData(Map<Object, Object> data) {
        /**
         * Helper function that translates Map to HttpRequest.BodyPublisher (used as a POST body)
         */
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }

    public HttpResponse<String> get(String uri) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseURL + uri))
                .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
                .build();

        return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }

    // POST Using String
    public HttpResponse<String> postFromString(String uri, String data) throws IOException, InterruptedException {
        return basicPost(uri, HttpRequest.BodyPublishers.ofString(data));
    }

    // POST using Map instead (like HashMap)
    public HttpResponse<String> postFromMap(String uri, Map<Object, Object> data) throws IOException, InterruptedException {
        return basicPost(uri, ofFormData(data));
    }

    private HttpResponse<String> basicPost(String uri, HttpRequest.BodyPublisher data) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(data)
                .uri(URI.create(baseURL + uri))
                .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }
}
