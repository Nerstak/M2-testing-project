package utils;

import java.sql.*;

public class DatabaseHandler {
    Connection c = null;


    public DatabaseHandler() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:./backend/db.sqlite3");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public int queryCount(String s) {
        try {
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(s);
            int rowCount = 0;
            if(rs.next()) {
                rowCount++;
            }
            return rowCount;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
