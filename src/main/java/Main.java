import models.AddEmployeeHttp;
import models.ResetDBHttp;
import utils.DatabaseHandler;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        DatabaseHandler dh = new DatabaseHandler();
        System.out.println(dh.queryCount("SELECT * FROM hr_employee"));
        /*
        (new ResetDBHttp()).resetDB();
        (new AddEmployeeHttp()).addEmptyEmployee();*/
    }
}
