import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.AddEmployee;
import models.EditEmployees;
import models.ResetDB;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PromoteEmployeeTest {
    private static Playwright playwright;
    private static Browser browser;

    @BeforeAll
    static void init() {
        playwright = Playwright.create();
        browser = playwright.firefox().launch();
    }

    @AfterAll
    static void close() {
        if(playwright != null) {
            playwright.close();
            playwright = null;
        }
    }

    @BeforeEach
    void initData() {
        Page page = browser.newPage();

        ResetDB resetDB = new ResetDB(page);
        resetDB.reset();

        AddEmployee addEmployee = new AddEmployee(page);
        addEmployee.navigate();

        HashMap<String, String> tmp = new HashMap<>();
        tmp.put("name", "Peter");
        tmp.put("email", "x@x.c");
        tmp.put("address1", "adr1");
        tmp.put("address2", "adr2");
        tmp.put("city", "myCity");
        tmp.put("zipCode", "09");
        tmp.put("hiringDate", "01/01/01");
        tmp.put("jobTitle", "MyTitle");

        addEmployee.fillFields(tmp);
        addEmployee.addEmployee();
    }

    @Test
    void shouldPromoteEmployee() {
        Page page = browser.newPage();
        EditEmployees editEmployees = new EditEmployees(page);
        editEmployees.navigate();
        editEmployees.editEmployee("Peter");
        editEmployees.startPromote();
        editEmployees.validatePromote();

        assertEquals("yes",page.innerText("tr:has(td:text(\"Peter\")) >> :nth-child(3)"));
    }
}
