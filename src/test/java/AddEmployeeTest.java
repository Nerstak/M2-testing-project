import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.AddEmployee;
import models.ResetDB;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddEmployeeTest {
    private static Playwright playwright;
    private static Browser browser;

    @BeforeAll
    static void init() {
        playwright = Playwright.create();
        browser = playwright.firefox().launch();
    }

    @AfterAll
    static void close() {
        if(playwright != null) {
            playwright.close();
            playwright = null;
        }
    }

    @BeforeEach
    void initData() {
        Page page = browser.newPage();
        ResetDB resetDB = new ResetDB(page);
        resetDB.reset();
    }

    @Test
    void shouldRefuseEmptyField() {
        Page page = browser.newPage();
        AddEmployee addEmployee = new AddEmployee(page);
        addEmployee.navigate();
        addEmployee.addEmployee();

        assertEquals("name cannot be blank\n" +
                "email cannot be blank\n" +
                "address_line1 cannot be blank\n" +
                "address_line2 cannot be blank\n" +
                "city cannot be blank\n" +
                "zip_code cannot be blank\n" +
                "hiring_date cannot be blank\n" +
                "job_title cannot be blank", page.innerText(".messages"));
    }
}
