import models.AddEmployeeHttp;
import models.ResetDBHttp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.DatabaseHandler;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddEmployeeHttpTest {
    @BeforeEach
    void initData() throws URISyntaxException {
        (new ResetDBHttp()).resetDB();
    }

    @Test
    // Should fail
    void shouldRefuseEmptyField() throws URISyntaxException, IOException, InterruptedException {
        (new AddEmployeeHttp()).addEmptyEmployee();

        assertEquals(0, new DatabaseHandler().queryCount("SELECT * FROM hr_employee"));
    }
}
