# Software Testing project - Team E


## Documents

[Journal de bord](https://docs.google.com/document/d/1IA6N91ks0HZDe4TJ9H4e732n2CCJqcladK-CrhP9_BU/edit?usp=sharing)

[Testing plan & runs](https://docs.google.com/spreadsheets/d/1qSUJFbamlLs61P6YT8STkSRR9yX4qFqe/edit?usp=sharing&ouid=116955860784380130992&rtpof=true&sd=true)

## What would it take to use white-box testing
In our case, we are a team testing the code quality of an application. While the application is relatively simple, it is not the case in real life products:
- It would require letting QA members access the source code (potential security problems in case of insecured passwords, files, etc.)
- It would require SE to possibly explain the code to QA members
- It would require setting a pre-production environment for QA teams (rather than just test/prod environments)

## Comparisons of testing methods
### Manual UI testing
#### Pros
- Close to the final user experience
- Cover e2e communication with other systems
- Catch high severity bugs (or at least, blocking ones)
- Cover UI testing 👍
- Easy to do, and flexible
#### Cons
- Time-consuming (need a human)
- Hard to replicate
- Hard to handle for complex cases
- May not detect all defects (we test interface + business logic at the same time)

### Automated UI testing
#### Pros
- Simulate user interaction, close to final experience
- Cover e2e communication with other systems
- Catch high severity bugs (or at least, blocking ones)
#### Cons
- May break easily (depends a lot on the UI) on fast changing interface
- Slow to run
- May not detect all defects (we test interface + business logic at the same time)

### Automated testing against backend
#### Pros
- We only test business logic
- In depth testing (we can cover more business case, and their edge cases)
- Faster run time compared to UI tests
- Easily integrable in TDD 😋
#### Cons
- Far from final user interface
- Not accessible requires a higher comprehension of the system (technical or logical point of view)
- May take time to write tests covering all cases
